# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-14 17:54+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: catalog/fields.py:23
msgid "Filetype not supported."
msgstr "ಫೈಲ್‌ಟೈಪ್ ಬೆಂಬಲಿಸುವುದಿಲ್ಲ."

#: catalog/templates/catalog/base.html:71
msgid "free games BOOM"
msgstr "ಉಚಿತ ಆಟಗಳು BOOM"

#: catalog/templates/catalog/base.html:78
#: catalog/templates/catalog/base.html:133
msgid "Find more 100000 games"
msgstr "ಇನ್ನಷ್ಟು 100000 ಆಟಗಳನ್ನು ಹುಡುಕಿ"

#: catalog/templates/catalog/base.html:79
#: catalog/templates/catalog/base.html:134
#: catalog/templates/catalog/search.html:7
msgid "Search"
msgstr "ಹುಡುಕಾಟ"

#: catalog/templates/catalog/base.html:97
#: catalog/templates/catalog/inclusions/sort_block.html:6
msgid "New"
msgstr "ಹೊಸ"

#: catalog/templates/catalog/base.html:103
msgid "Popular"
msgstr "ಜನಪ್ರಿಯ"

#: catalog/templates/catalog/base.html:114
msgid "Favourite <br>games"
msgstr "ನೆಚ್ಚಿನ <br>ಆಟಗಳು"

#: catalog/templates/catalog/base.html:125
msgid "Last <br>played"
msgstr "ಕೊನೆಯ <br>ಆಡಿದರು"

#: catalog/templates/catalog/category.html:95
msgid "Information"
msgstr "ಮಾಹಿತಿ"

#: catalog/templates/catalog/game.html:90
msgid "Category"
msgstr "ವರ್ಗ"

#: catalog/templates/catalog/game.html:105
msgid "Game"
msgstr "ಆಟ"

#: catalog/templates/catalog/game.html:131
#: catalog/templates/catalog/game.html:133
msgid "PLAY"
msgstr "ಆಡಲು"

#: catalog/templates/catalog/game.html:142
msgid "This game is not available on mobile."
msgstr "ಈ ಆಟವು ಮೊಬೈಲ್‌ನಲ್ಲಿ ಲಭ್ಯವಿಲ್ಲ."

#: catalog/templates/catalog/game.html:174
msgid "Controls"
msgstr "ನಿಯಂತ್ರಣಗಳು"

#: catalog/templates/catalog/game.html:181
msgid "Video"
msgstr "ವೀಡಿಯೊ"

#: catalog/templates/catalog/inclusions/_include_game_list.html:19
msgid "Play!"
msgstr "ಪ್ಲೇ!"

#: catalog/templates/catalog/inclusions/category_menu.html:9
msgid "TOP CATEGORIES"
msgstr "ಉನ್ನತ ವರ್ಗಗಳು"

#: catalog/templates/catalog/inclusions/game_block.html:5
msgid "votes"
msgstr "ಮತಗಳು"

#: catalog/templates/catalog/inclusions/game_block.html:11
msgid "Doesn’t  <br>work?"
msgstr "<br> ಕೆಲಸ ಮಾಡುವುದಿಲ್ಲವೇ?"

#: catalog/templates/catalog/inclusions/game_block.html:17 catalog/views.py:226
msgid "Added"
msgstr "ಸೇರಿಸಲಾಗಿದೆ"

#: catalog/templates/catalog/inclusions/game_block.html:17
msgid "Add to <br>favorites"
msgstr "<br> ಮೆಚ್ಚಿನವುಗಳಿಗೆ ಸೇರಿಸಿ"

#: catalog/templates/catalog/inclusions/game_block.html:35
msgid "Share"
msgstr "ಹಂಚಿಕೊಳ್ಳಿ"

#: catalog/templates/catalog/inclusions/game_block.html:41
msgid "Fullscreen"
msgstr "ಪೂರ್ಣ ಪರದೆ"

#: catalog/templates/catalog/inclusions/popup-games.html:7
msgid "Clear list"
msgstr "ಪಟ್ಟಿ ತೆರವುಗೊಳಿಸಿ"

#: catalog/templates/catalog/inclusions/sort_block.html:3
msgid "Sort by"
msgstr "ವಿಂಗಡಿಸು"

#: catalog/templates/catalog/inclusions/sort_block.html:3
#: catalog/templates/catalog/inclusions/sort_block.html:5
msgid "Most Popular"
msgstr "ಅತ್ಯಂತ ಜನಪ್ರಿಯ"

#: catalog/templates/catalog/inclusions/sort_block.html:7
msgid "A-Z"
msgstr "A-Z"

#: catalog/templates/catalog/tags.html:6
msgid "Tags"
msgstr "ಟ್ಯಾಗ್ಗಳು"

#: catalog/templates/catalog/tags.html:11
msgid "Popular tags"
msgstr "ಜನಪ್ರಿಯ ಟ್ಯಾಗ್‌ಗಳು"

#: catalog/templatetags/catalog_tags.py:45
msgid "MOST POPULAR GAMES"
msgstr "ಹೆಚ್ಚು ಜನಪ್ರಿಯ ಆಟಗಳು"

#: catalog/templatetags/catalog_tags.py:64
msgid "POPULAR TAGS"
msgstr "ಜನಪ್ರಿಯ ಟ್ಯಾಗ್‌ಗಳು"

#: catalog/templatetags/catalog_tags.py:87
msgid "MOST POPULAR"
msgstr "ಅತ್ಯಂತ ಜನಪ್ರಿಯ"

#: catalog/templatetags/catalog_tags.py:156
msgid "FGM RECOMMENDED!"
msgstr "FGM ಶಿಫಾರಸು ಮಾಡಲಾಗಿದೆ!"

#: catalog/templatetags/catalog_tags.py:160
#, python-format
msgid "MORE %(category)s"
msgstr "ಇನ್ನಷ್ಟು %(category)s"

#: catalog/templatetags/catalog_tags.py:187
msgid "LAST PLAYED"
msgstr "ಕೊನೆಯದಾಗಿ ಆಡಲಾಗಿದೆ"

#: catalog/templatetags/catalog_tags.py:282 catalog/views.py:71
msgid "New games"
msgstr "ಹೊಸ ಆಟಗಳು"

#: catalog/views.py:70
msgid "New games - Free Games Boom.com"
msgstr "ಹೊಸ ಆಟಗಳು - Free Games Boom.com"

#: catalog/views.py:74
msgid "Popular games - Free Games Boom.com"
msgstr "ಜನಪ್ರಿಯ ಆಟಗಳು - Free Games Boom.com"

#: catalog/views.py:75
msgid "Popular games"
msgstr "ಜನಪ್ರಿಯ ಆಟಗಳು"

#: catalog/views.py:99
#, fuzzy, python-format
#| msgid "Popular games"
msgid "Similar %s games"
msgstr "ಜನಪ್ರಿಯ ಆಟಗಳು"

#: catalog/views.py:191
msgid "Favourite games"
msgstr "ನೆಚ್ಚಿನ ಆಟಗಳು"

#: catalog/views.py:194
msgid "Last Played"
msgstr "ಕೊನೆಯದಾಗಿ ಆಡಲಾಗಿದೆ"

#: flatpages/apps.py:7
msgid "Flat Pages"
msgstr "ಫ್ಲಾಟ್ ಪುಟಗಳು"

#: flatpages/models.py:12
msgid "Title"
msgstr "ಶೀರ್ಷಿಕೆ"

#: flatpages/models.py:14
msgid "Text"
msgstr "ಪಠ್ಯ"

#: flatpages/models.py:15
msgid "Active"
msgstr "ಸಕ್ರಿಯ"

#: flatpages/models.py:16
msgid "Modified"
msgstr "ಮಾರ್ಪಡಿಸಲಾಗಿದೆ"

#: flatpages/models.py:19
msgid "Static pages"
msgstr "ಸ್ಥಾಯೀ ಪುಟಗಳು"

#: flatpages/models.py:20
msgid "Static page"
msgstr "ಸ್ಥಾಯೀ ಪುಟ"

#: freegamesboom/settings.py:155
msgid "Afrikaans"
msgstr "ಆಫ್ರಿಕಾನ್ಸ್"

#: freegamesboom/settings.py:156
msgid "Arabic"
msgstr "ಅರೇಬಿಕ್"

#: freegamesboom/settings.py:157
msgid "Azerbaijani"
msgstr "ಅಜೆರ್ಬೈಜಾನಿ"

#: freegamesboom/settings.py:158
msgid "Bulgarian"
msgstr "ಬಲ್ಗೇರಿಯನ್"

#: freegamesboom/settings.py:159
msgid "Belarusian"
msgstr "ಬೆಲರೂಸಿಯನ್"

#: freegamesboom/settings.py:160
msgid "Bengali"
msgstr "ಬಂಗಾಳಿ"

#: freegamesboom/settings.py:161
msgid "Bosnian"
msgstr "ಬೋಸ್ನಿಯನ್"

#: freegamesboom/settings.py:162
msgid "Catalan"
msgstr "ಕೆಟಲಾನ್"

#: freegamesboom/settings.py:163
msgid "Czech"
msgstr "ಜೆಕ್"

#: freegamesboom/settings.py:164
msgid "Welsh"
msgstr "ವೆಲ್ಷ್"

#: freegamesboom/settings.py:165
msgid "Danish"
msgstr "ಡ್ಯಾನಿಶ್"

#: freegamesboom/settings.py:166
msgid "German"
msgstr "ಜರ್ಮನ್"

#: freegamesboom/settings.py:167
msgid "Greek"
msgstr "ಗ್ರೀಕ್"

#: freegamesboom/settings.py:168
msgid "English"
msgstr "ಆಂಗ್ಲ"

#: freegamesboom/settings.py:169
msgid "Esperanto"
msgstr "ಎಸ್ಪೆರಾಂಟೊ"

#: freegamesboom/settings.py:170
msgid "Spanish"
msgstr "ಸ್ಪ್ಯಾನಿಷ್"

#: freegamesboom/settings.py:171
msgid "Estonian"
msgstr "ಎಸ್ಟೋನಿಯನ್"

#: freegamesboom/settings.py:172
msgid "Finnish"
msgstr "ಫಿನ್ನಿಷ್"

#: freegamesboom/settings.py:173
msgid "French"
msgstr "ಫ್ರೆಂಚ್"

#: freegamesboom/settings.py:174
msgid "Irish"
msgstr "ಐರಿಶ್"

#: freegamesboom/settings.py:175
msgid "Galician"
msgstr "ಗ್ಯಾಲಿಶಿಯನ್"

#: freegamesboom/settings.py:176
msgid "Hebrew"
msgstr "ಹೀಬ್ರೂ"

#: freegamesboom/settings.py:177
msgid "Hindi"
msgstr "ಹಿಂದಿ"

#: freegamesboom/settings.py:178
msgid "Croatian"
msgstr "ಕ್ರೊಯೇಷಿಯಾದ"

#: freegamesboom/settings.py:179
msgid "Hungarian"
msgstr "ಹಂಗೇರಿಯನ್"

#: freegamesboom/settings.py:180
msgid "Armenian"
msgstr "ಅರ್ಮೇನಿಯನ್"

#: freegamesboom/settings.py:182
msgid "Italian"
msgstr "ಇಟಾಲಿಯನ್"

#: freegamesboom/settings.py:183
msgid "Japanese"
msgstr "ಜಪಾನೀಸ್"

#: freegamesboom/settings.py:184
msgid "Georgian"
msgstr "ಜಾರ್ಜಿಯನ್"

#: freegamesboom/settings.py:186
msgid "Kazakh"
msgstr "ಕ Kazakh ಕ್"

#: freegamesboom/settings.py:187
msgid "Khmer"
msgstr "ಖಮೇರ್"

#: freegamesboom/settings.py:188
msgid "Kannada"
msgstr "ಕನ್ನಡ"

#: freegamesboom/settings.py:189
msgid "Korean"
msgstr "ಕೊರಿಯನ್"

#: freegamesboom/settings.py:190
msgid "Luxembourgish"
msgstr "ಲಕ್ಸೆಂಬರ್ಗ್"

#: freegamesboom/settings.py:191
msgid "Lithuanian"
msgstr "ಲಿಥುವೇನಿಯನ್"

#: freegamesboom/settings.py:192
msgid "Latvian"
msgstr "ಲಟ್ವಿಯನ್"

#: freegamesboom/settings.py:193
msgid "Macedonian"
msgstr "ಮೆಸಿಡೋನಿಯನ್"

#: freegamesboom/settings.py:194
msgid "Malayalam"
msgstr "ಮಲಯಾಳಂ"

#: freegamesboom/settings.py:195
msgid "Mongolian"
msgstr "ಮಂಗೋಲಿಯನ್"

#: freegamesboom/settings.py:196
msgid "Marathi"
msgstr "ಮರಾಠಿ"

#: freegamesboom/settings.py:197
msgid "Burmese"
msgstr "ಬರ್ಮೀಸ್"

#: freegamesboom/settings.py:198
msgid "Nepali"
msgstr "ನೇಪಾಳಿ"

#: freegamesboom/settings.py:199
msgid "Dutch"
msgstr "ಡಚ್"

#: freegamesboom/settings.py:200
msgid "Ossetic"
msgstr "ಒಸ್ಸೆಟಿಕ್"

#: freegamesboom/settings.py:201
msgid "Punjabi"
msgstr "ಪಂಜಾಬಿ"

#: freegamesboom/settings.py:202
msgid "Polish"
msgstr "ಪೋಲಿಶ್"

#: freegamesboom/settings.py:203
msgid "Portuguese"
msgstr "ಪೋರ್ಚುಗೀಸ್"

#: freegamesboom/settings.py:204
msgid "Romanian"
msgstr "ರೊಮೇನಿಯನ್"

#: freegamesboom/settings.py:205
msgid "Russian"
msgstr "ರಶಿಯನ್"

#: freegamesboom/settings.py:206
msgid "Slovak"
msgstr "ಸ್ಲೋವಾಕ್"

#: freegamesboom/settings.py:207
msgid "Slovenian"
msgstr "ಸ್ಲೋವೇನಿಯನ್"

#: freegamesboom/settings.py:208
msgid "Albanian"
msgstr "ಅಲ್ಬೇನಿಯನ್"

#: freegamesboom/settings.py:209
msgid "Serbian"
msgstr "ಸರ್ಬಿಯನ್"

#: freegamesboom/settings.py:210
msgid "Swedish"
msgstr "ಸ್ವೀಡಿಷ್"

#: freegamesboom/settings.py:211
msgid "Swahili"
msgstr "ಸ್ವಹಿಲಿ"

#: freegamesboom/settings.py:212
msgid "Tamil"
msgstr "ತಮಿಳು"

#: freegamesboom/settings.py:213
msgid "Telugu"
msgstr "ತೆಲುಗು"

#: freegamesboom/settings.py:214
msgid "Thai"
msgstr "ಥಾಯ್"

#: freegamesboom/settings.py:215
msgid "Turkish"
msgstr "ಟರ್ಕಿಶ್"

#: freegamesboom/settings.py:216
msgid "Tatar"
msgstr "ಟಾಟರ್"

#: freegamesboom/settings.py:218
msgid "Ukrainian"
msgstr "ಉಕ್ರೇನಿಯನ್"

#: freegamesboom/settings.py:219
msgid "Urdu"
msgstr "ಉರ್ದು"

#: freegamesboom/settings.py:220
msgid "Vietnamese"
msgstr "ವಿಯೆಟ್ನಾಮೀಸ್"

#: freegamesboom/settings.py:221
msgid "Simplified Chinese"
msgstr "ಸರಳೀಕೃತ ಚೈನೀಸ್"

#: templates/404.html:18
msgid "404 Error"
msgstr "404 ದೋಷ"

#: templates/404.html:19
msgid "Oops. This page does not exist."
msgstr "ಅಯ್ಯೋ. ಈ ಪುಟ ಅಸ್ತಿತ್ವದಲ್ಲಿಲ್ಲ."

#: templates/el_pagination/show_more.html:5
msgid "more"
msgstr "ಹೆಚ್ಚು"

#: templates/el_pagination/show_pages.html:6
msgid "All"
msgstr "ಎಲ್ಲಾ"

#~ msgid "Indonesian"
#~ msgstr "ಇಂಡೋನೇಷಿಯನ್"

#~ msgid "Kabyle"
#~ msgstr "ಕಬೈಲ್"

#~ msgid "Udmurt"
#~ msgstr "ಉಡ್ಮರ್ಟ್"
