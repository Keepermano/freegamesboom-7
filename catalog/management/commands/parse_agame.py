# -*- coding: utf-8 -*-
import requests
import time
import json
import math

from tqdm import tqdm

from django.core.management.base import BaseCommand
from lxml import html

from catalog.models import GameParse, GameErrorParse

site = 'http://www.agame.com'


class Command(BaseCommand):

    def get_categories_links(self):
        r = requests.get(url = '%s/allcategories' % site)
        tree = html.fromstring(r.text)
        data = []
        for item in tree.findall(".//ul[@class='label_list']/li/a"):
            data.append('%s%s' % (site, item.attrib['href'].strip()))
        return data

    def handle(self, *args, **options):
        self.get_categories_links()

        for category_url in self.get_categories_links():

            r = requests.get(category_url)
            cat_tree = html.fromstring(r.text)

            try:
                html_pages = cat_tree.xpath('//nav[@class="pagination clearfix"]/p')[0]
                total_pages = math.ceil(int(html_pages.text.split(' ')[2]) / 36)
            except:
                total_pages = 2

            for x in range(1, total_pages):
                cat_url = '%s?page=%s' % (category_url, x)
                print(cat_url)
                r = requests.get(cat_url)
                cat_tree = html.fromstring(r.text)

                for game in cat_tree.xpath('//ul[@class="grid-row"]/li/div[@class="tile"]'):
                    game_href = game.findall('a[@class="tile-title"]')[0].attrib['href']
                    game_url = '%s%s' % (site, game_href)
                    image_url = game.findall('div[@class="tile-thumbnail"]/img')[0].attrib['src']

                    slug = game_href.split('/')[2]
                    try:
                        GameParse.objects.get(slug=slug)
                    except GameParse.DoesNotExist:
                        try:
                            r = requests.get(game_url)
                            game_tree = html.fromstring(r.text)
                            try:
                                title = game_tree.xpath('//h1')[0].text
                            except IndexError:
                                title = ''
                            try:
                                category = game_tree.xpath('//ul[@class="breadcrumb"]/li')[1].text_content()
                            except IndexError:
                                category = ''
                            try:
                                subcategory = game_tree.xpath('//ul[@class="breadcrumb"]/li')[2].text_content()
                            except:
                                subcategory = ''
                            try:
                                description = game_tree.xpath('//div[@class="description"]/p')[0].text
                            except IndexError:
                                description = ''
                            tags = []
                            for tag in game_tree.xpath('//ul[@class="labels"]/li'):
                                tags.append(tag.text_content())

                            code = game_tree.xpath('//*[@id="game-integration"]/script[2]/text()')
                            if not code:
                                code = game_tree.xpath('//*[@id="game-integration"]/script/text()')

                            try:
                                GameParse.objects.update_or_create(slug=slug, defaults={
                                    'title': title,
                                    'category': category,
                                    'image': image_url,
                                    'subcategory': subcategory,
                                    'description': description,
                                    'url': game_url,
                                    'code': code,
                                    'tags': ','.join(tags),
                                })
                            except:
                                print(slug)
                        except requests.exceptions.ConnectionError:
                            try:
                                GameErrorParse.objects.update_or_create(url=game_url)
                            except:
                                pass
                            print("Connection refused by the server..")
                            print("Let me sleep for 5 seconds")
                            print("ZZzzzz...")
                            time.sleep(5)
                            print("Was a nice sleep, now let me continue...")
                            continue
                    else:
                        continue
