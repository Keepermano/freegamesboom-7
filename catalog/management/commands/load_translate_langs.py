# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from catalog.models import TranslateLanguage
from django.conf import settings


class Command(BaseCommand):

    def handle(self, *args, **options):
        objects = TranslateLanguage.objects.all().values_list('slug', flat=True)
        for o in settings.LANGUAGES:
            if o[0] not in objects:
                TranslateLanguage.objects.get_or_create(
                    slug=o[0],
                    name=o[1],
                )
